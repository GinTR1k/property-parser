import logging


def exception_handler(func):
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except Exception as e:
            logging.getLogger().exception(e)
            return None
    return wrapper
