from typing import List


class SearchProfile:
    name: str
    age: int
    has_phone_numbers: bool
    locations: List[str]
    worked_at: List[str]
    relatives: List[str]
    studied_at: List[str]

    def __init__(
            self, name: str, age: int = None, has_phone_numbers: bool = None,
            locations: List[str] = None, worked_at: List[str] = None,
            relatives: List[str] = None, studied_at: List[str] = None
    ):
        self.name = name
        self.age = age if age else None
        self.has_phone_numbers = (
            has_phone_numbers if has_phone_numbers else False)
        self.locations = locations if locations else []
        self.worked_at = worked_at if worked_at else []
        self.relatives = relatives if relatives else []
        self.studied_at = studied_at if studied_at else []

    def to_dict(self):
        return {
            'name': self.name,
            'age': self.age,
            'has_phone_numbers': self.has_phone_numbers,
            'locations': self.locations,
            'worked_at': self.worked_at,
            'relatives': self.relatives,
            'studied_at': self.studied_at,
        }


class ResponseData:
    results: List[SearchProfile]

    def __init__(self, results: List[SearchProfile] = None):
        self.results = results if results else []
        self.count = len(self.results)

    def to_dict(self):
        return {
            'count': self.count,
            'results': [profile.to_dict() for profile in self.results],
        }
