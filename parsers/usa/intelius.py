import asyncio
import random
import re
from asyncio.events import AbstractEventLoop
from functools import wraps
from typing import List, Optional, Union

from aiohttp import ClientResponse, ClientResponseError, request
from aiohttp_socks import ProxyConnector
from bs4 import BeautifulSoup, element
from yarl import URL

from config import Config
from models import ResponseData, SearchProfile
from utils import exception_handler


class InteliusParser:
    SEARCH_PAGE_PATH: str = '/people-search'
    URL: str = 'https://www.intelius.com'

    def __init__(
            self, config: Config, searching_name: str, cookies: dict,
            mirror: str = None, proxy: str = None,
            loop: AbstractEventLoop = None
    ):
        self._config = config
        self._loop = loop or asyncio.get_event_loop()
        self.searching_name = searching_name.replace('+', ' ')
        self.cookies = cookies
        self.mirror = mirror
        self.proxy = proxy

        self._proxy_connector: Optional[ProxyConnector] = None
        if proxy:
            self._proxy_connector = ProxyConnector.from_url(proxy)

    async def close(self):
        if self._proxy_connector:
            await self._proxy_connector.close()

    async def parse(self) -> Optional[ResponseData]:
        html = await self._get_search_page(self.searching_name)

        first_page_soup = BeautifulSoup(html, 'lxml')
        soup_pages: List[BeautifulSoup] = [first_page_soup]
        result: List[SearchProfile] = []

        # TODO: сделать парсинг страниц
        # for path in self._get_paths_from_pagination(first_page_soup):
        #     soup_pages.append(BeautifulSoup(
        #         await self._get_search_page(path),
        #         'lxml'
        #     ))

        for soup in soup_pages:
            soup_profiles = self._get_profiles_html(soup)
            for soup_profile in soup_profiles:
                result.append(self._parse_profile_from_html(soup_profile))

        return ResponseData(result)

    @staticmethod
    def _get_paths_from_pagination(soup: BeautifulSoup) -> List[str]:
        pagination_elements: element.ResultSet = soup.find_all(
            'li', class_='pagination-link')
        return [a_tag.find('a') for a_tag in pagination_elements]

    @staticmethod
    def _get_profiles_html(soup: BeautifulSoup) -> Union[
            element.ResultSet, list]:
        soup_results: element.Tag = soup.find('div', class_='results')
        if not soup_results:
            return []

        return soup_results.select('div.profile.show-for-large')

    @staticmethod
    def _parse_profile_from_html(profile) -> SearchProfile:
        name = profile.find('div', class_='profile-name').string

        age = ''.join(
            symbol
            for symbol in
            profile.find('div', class_='profile-age').find('span').string
            if symbol.isdigit()
        )
        if age:
            age = int(age)
        else:
            age = None

        has_phones_numbers = bool(
            profile.find('div', class_='profile-phone')
            .find_all('span', class_='show-more-item')
        )

        soup_locations = (
            profile.find('div', class_='profile-locations')
            .find_all('span', class_='show-more-item')
        )
        locations = [str(location.string) for location in soup_locations]

        soup_worked_at = (
            profile.find('div', class_='profile-work')
            .find_all('span', class_='show-more-item')
        )
        worked_at = [str(work.string) for work in soup_worked_at]

        soup_relatives = (
            profile.find('div', class_='profile-relatives')
            .find_all('span', class_='show-more-item')
        )
        relatives = [str(relative.string) for relative in soup_relatives]

        soup_studied_at = (
            profile.find('div', class_='profile-study')
            .find_all('span', class_='show-more-item')
        )
        studied_at = [str(university.string) for university in soup_studied_at]

        return SearchProfile(
            name, age, has_phones_numbers,
            locations, worked_at, relatives, studied_at
        )

    async def _get_search_page(
            self, searching_name: str = None, path: str = None
    ) -> str:
        if not searching_name and path:
            raise ValueError(
                'Not passed arguments `searching_name` and `path`')

        url = self.mirror if self.mirror else self.URL
        if path:
            url += path
        else:
            url += (
                self.SEARCH_PAGE_PATH
                + '/' + searching_name.replace('+', '-').replace(' ', '-')
            )

        async with request(
                method='get', url=URL(url, encoded=True), cookies=self.cookies,
                connector=self._proxy_connector, headers={
                    'User-Agent': random.choice(self._config.USER_AGENTS)
                }, loop=self._loop
        ) as response:
            print(response.request_info)
            self._validate_response(response)
            return await response.text()

    @exception_handler
    @wraps(_get_search_page)
    async def _get_search_page_task(self, *args, **kwargs):
        await self._get_search_page(*args, **kwargs)

    @staticmethod
    def _validate_response(response: ClientResponse):
        if response.status != 200:
            raise ClientResponseError(
                request_info=response.request_info,
                history=response.history,
                status=response.status,
                message='Can\'t parse intelius.com! Wrong HTTP Status '
                        'Code! Try to pass different `datadome` cookie',
            )
