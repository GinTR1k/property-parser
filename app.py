import argparse
import asyncio
import json

from config import Config
from parsers.usa.intelius import InteliusParser

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('searching_name', help='Name')
    arg_parser.add_argument(
        'datadome', help='Datadome cookie from intelius.com')
    arg_parser.add_argument(
        '--mirror', help='Mirror URL for www.intelius.com',
        required=False, default=None)
    arg_parser.add_argument(
        '--proxy', help='Proxy', required=False, default=None)
    args = arg_parser.parse_args()
    loop = asyncio.get_event_loop()

    intelius_parser = InteliusParser(
        config=Config(), searching_name=args.searching_name,
        cookies={'datadome': args.datadome}, mirror=args.mirror,
        proxy=args.proxy, loop=loop
    )

    try:
        data = loop.run_until_complete(intelius_parser.parse())
    finally:
        loop.run_until_complete(intelius_parser.close())
        loop.close()

    json_data = json.dumps(data.to_dict())
    print(json_data)
