from typing import List

from environs import Env

env = Env()
env.read_env()


class Config:
    USER_AGENTS: List[str] = env.list('USER_AGENTS', [
        'Mozilla/5.0 (X11; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0'
    ])
